package com.burnier.timeclockproject.company;

import com.burnier.timeclockproject.utils.WorkingDay;

import java.io.Serializable;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;


public class Employee implements Serializable {

    private int id = 0;
    private String firstName;
    private String lastName;
    private String address;
    private int age;
    private Sex gender;
    private Department department;
    private LocalTime timeArrival; //TODO REFACTOR WITH A LIST<DAY> workingHoursByWeek
    private LocalTime timeEnding;
    private Role role;
    private final LinkedList<WorkingDay> workingDay = new LinkedList<>();
    private static int count = 1;
    /**
     * Constructor of an employee
     * @param firstName the first name of the employee
     * @param lastName the last name of the employee
     * @param gender the gender of the employee
     * @param address the address of the employee
     * @param age the age of the employee
     * @param department the department of the employee
     * @param timeArrival the arrival time of the employee
     * @param timeEnding the time ending of the employee
     */
    public Employee(String firstName, String lastName,Sex gender, String address, int age, Department department, String timeArrival, String timeEnding)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.gender = gender;
        this.id = count;
        this.department = department;
        this.timeArrival = LocalTime.parse(timeArrival, formatter);
        this.timeEnding = LocalTime.parse(timeEnding, formatter);
        this.age = age;
        this.role = Role.NOT_A_MANAGER;
        count++;
    }

    /**
     * Constructor of an employee
     * @param firstName the first name of the employee
     * @param lastName the last name of the employee
     * @param gender the gender of the employee
     * @param address the address of the employee
     * @param age the age of the employee
     * @param department the department of the employee
     * @param timeArrival the time arrival of the employee
     * @param timeEnding the time ending of the employee
     */
    public Employee(String firstName, String lastName,Sex gender, String address, int age, Department department, LocalTime timeArrival, LocalTime timeEnding)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.gender = gender;
        this.id = count;
        this.department = department;
        this.timeArrival = timeArrival;
        this.timeEnding = timeEnding;
        this.age = age;
        this.role = Role.NOT_A_MANAGER;
        count++;
    }
    /**
     * Constructor of an employee
     * @param firstName the first name of the employee
     * @param lastName the last name of the employee
     */
    public Employee(String firstName, String lastName)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = count;
        this.role = Role.NOT_A_MANAGER;
        count++;
    }

    /**
     * Constructor per default of Employee
     */
    public Employee() {
    }

    public Employee(String firstName, String lastName, Department department, Role role) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
        this.role = role;
    }

    /**
     * Get the name of the department
     * @return the name of the department
     */
    public String getNameDepartment()
    {
        return department.getNameDepartment();
    }

    /**
     * Set the address
     * @param address the address of the employee
     */
    public void setAddress(String address) {
        this.address = address;
    }
    /**
     * Set the age
     * @param age the age of the employee
     */
    public void setAge(int age) {
        this.age = age;
    }
    /**
     * Set the id
     * @param id the id of the employee
     */
    //DO NOT MODIFY THE ID MANUALLY OR IT COULD CREATE PROBLEMS BETWEEN ID
    public void setId(int id) {
        this.id = id;
    }
    /**
     * Set the department
     * @param department the department of the employee
     */
    public void setDepartment(Department department) {
        this.department = department;
    }
    /**
     * Set the time arrival
     * @param timeArrival the time arrival of the employee
     */
    public void setTimeArrival(LocalTime timeArrival) {
        this.timeArrival = timeArrival;
    }

    /**
     * Set the time ending
     * @param timeEnding the time ending of the employee
     */
    public void setTimeEnding(LocalTime timeEnding) {
        this.timeEnding = timeEnding;
    }
    /**
     * Set the gender
     * @param gender the gender of the employee
     */
    public void setGender(Sex gender)
    {
        this.gender = gender;
    }

    /**
     * Get the first name
     * @return the first name
     * @see String
     */
    public String getFirstName()
    {
        return firstName;
    }
    /**
     * Get the last name
     * @return the last name
     * @see String
     */
    public String getLastName()
    {
        return lastName;
    }

    /**
     *  Get the full name
     * @return the full name
     * @see String
     */
    public String getName()
    {
        return firstName +" " + lastName;
    }
    /**
     * Get the gender
     * @return the sex
     * @see Sex
     */
    public Sex getGender() {
        return gender;
    }
    /**
     * Get the WorkingDay
     * @return the workingDay
     * @see LinkedList
     */
    public LinkedList<WorkingDay> getWorkingDay()
    {
        return workingDay;
    }

    /**
     * Get the id
     * @return the id
     * @see int
     */
    public int getId() {
        return id;
    }
    /**
     * Get the age
     * @return the age
     * @see int
     */
    public int getAge() {
        return age;
    }
    /**
     * Get the address
     * @return the address
     * @see String
     */
    public String getAddress() {
        return address;
    }
    /**
     * Get the time arrival
     * @return the time arrival
     * @see LocalTime
     */
    public LocalTime getTimeArrival() {
        return timeArrival;
    }
    /**
     * Get the time ending
     * @return the time ending
     * @see LocalTime
     */
    public LocalTime getTimeEnding() {
        return timeEnding;
    }
    /**
     * Get the department
     * @return the department
     * @see Department
     */
    public Department getDepartment() {
        return department;
    }

    /**
     * Set the role of the employee
     * @param role the role of the employee
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Get the role of the employee
     * @return the role
     * @see Role
     */
    public Role getRole() {
        return role;
    }

    /**
     * Print the information about the employee
     * @return the information about the employee
     * @see String
     */
    public String toString()
    {
        return "Employee's name :\n" + "ID : " + id + "\nDepartment : " + getDepartment().getNameDepartment() + "\nFirst Name : " + firstName + "\n" + "Last Name :" + lastName
                + "\nAddress : " + address + "\nSex : " + getGender()
                + "\nAge : " + age + "\nTime Arrival : "
                + timeArrival + "\nTime Ending : " + timeEnding + "\n";

    }
}