package com.burnier.timeclockproject.company;

import java.io.Serializable;
import java.util.LinkedList;

public class Department implements Serializable {
    private String departmentName;
    private Manager manager;
    private final LinkedList<Employee> listEmployee;

    /**
     * Constructor of a department
     *
     * @param name the name of the department
     */
    public Department(String name) {
        this.departmentName = name;
        this.manager = null;
        listEmployee = new LinkedList<>();
    }

    /**
     * Get the list of employees
     * @return the list of employees
     */
    public LinkedList<Employee> getListEmployee() {
        return listEmployee;
    }

    /**
     * Set the name of the department
     *
     * @param name the name of the department
     */
    public void setNameDepartment(String name) {
        this.departmentName = name;
    }

    /**
     * Get the name of the department
     *
     * @return String
     */
    public String getNameDepartment() {
        return departmentName;
    }


    /**
     * Get the manager of the department
     *
     * @return the object manager
     * @see Manager
     */
    public Manager getManager() {
        return manager;
    }

    /**
     * Add a manager
     *
     * @param manager the manager which will be added
     */
    public void addManager(Manager manager, Company company) throws Exception {
        this.manager = manager;
        listEmployee.add(manager); //TODO REFACTOR CREATE MANAGER LIST
    }

    /**
     * Add an employee to the list of employees
     * @param employee an Employee
     */
    public void addEmployee(Employee employee) {
        listEmployee.add(employee);
    }

    /**
     * Delete the manager leading the department
     */
    public void deleteManager() {
        listEmployee.remove(manager);
        manager = null;
    }

    /**
     * Set a manager
     *
     * @param manager the object manager
     */
    public void setManager(Manager manager) {
        this.manager = manager;
    }

    /**
     * Print the information about a department
     *
     * @return String
     */
    public String toString() {
        if (departmentName != null && listEmployee != null && manager != null) {
            return "Nom du département : " + departmentName + "\n" + "Nombre d'employées : " + listEmployee.size() + "\nLeader of the department : " + getManager().getLastName();
        }
        return null;
    }
}
