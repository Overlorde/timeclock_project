package com.burnier.timeclockproject.company;

import com.burnier.timeclockproject.utils.CheckInCheckOut;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.logging.Logger;

public class Company implements Serializable {
    private final LinkedList<Department> listOfDepartment;
    private final String companyName;
    private final String bossName;
    private final CheckInCheckOut checkInCheckOut = new CheckInCheckOut();
    private static final Logger LOGGER = Logger.getLogger(String.valueOf(Company.class));

    /**
     * Constructor of a company with default options
     */
    public Company() {
        this.companyName = "Default";
        this.bossName = "Default";
        listOfDepartment = new LinkedList<>();
    }

    /**
     * Constructor of a company with two arguments
     *
     * @param companyName the name of the company
     * @param bossName    the name of the boss
     */
    public Company(String companyName, String bossName) {
        this.companyName = companyName;
        this.bossName = bossName;
        listOfDepartment = new LinkedList<>();
    }

    /**
     * Get the checkInCheckOut object
     *
     * @return the checkInCheckOut object of the company
     * @see CheckInCheckOut
     */
    public CheckInCheckOut getCheckInCheckOut() {
        return checkInCheckOut;
    }

    /**
     * Research a department with a name
     *
     * @param departmentName the name of the department
     * @return one of the department of the company
     * @see Department
     */
    public Department searchDepartment(String departmentName) throws IllegalArgumentException {
        if(departmentName != null)
        for (Department department : listOfDepartment) {
            if (departmentName.equals(department.getNameDepartment())) {
                return department;
            }
        }
        return null;
    }

    /**
     * Allow to add a department to the company
     * Add a department
     *
     * @param department the department which will be added to the company
     */
    public void addDepartment(Department department) throws Exception {
        for (Department departmentIt : listOfDepartment) {
            if (department.getNameDepartment().equals(departmentIt.getNameDepartment())) {
                throw new Exception("Already a department with this name");
            }
        }
        listOfDepartment.add(department);
    }

    /**
     * Get the list of department
     *
     * @return the list of department
     * @see LinkedList
     */
    public LinkedList<Department> getListOfDepartment() {
        return listOfDepartment;
    }

}
