package com.burnier.timeclockproject.company;

import java.io.Serializable;

public class Manager extends Employee implements Serializable {
    private final String email;

    /**
     * Constructor of a Manager
     *
     * @param firstName   the first name of the manager
     * @param lastName    the last name of the manager
     * @param gender      the gender of the manager
     * @param role        the role of the manager
     * @param address     the address of the manager
     * @param age         the age of the manager
     * @param department  the department of the manager
     * @param timeArrival the time arrival of the manager
     * @param timeEnding  the time ending of the manager
     */
    public Manager(String firstName, String lastName, Sex gender, Role role, String address, int age, Department department, String timeArrival, String timeEnding) {
        super(firstName, lastName, gender, address, age, department, timeArrival, timeEnding);
        setRole(role);
        this.email = "NOT_IMPLEMENTED";
    }

    /**
     * Constructor of a Manager
     *
     * @param employee the object which contain the employee evolving to a new status
     * @param role     the role of the manager
     */
    public Manager(Employee employee, Role role) {
        super(employee.getFirstName(), employee.getLastName(), employee.getGender(), employee.getAddress(), employee.getAge(), employee.getDepartment(), employee.getTimeArrival(), employee.getTimeEnding());
        setRole(role);
        setId(employee.getId());
        this.email = "NOT_IMPLEMENTED";
    }

    /**
     * Constructor of a Manager
     *
     * @param firstName the first name of the manager
     * @param lastName  the last name of the manager
     * @param role      the role of the manager
     */
    public Manager(String firstName, String lastName, Role role, Department department) {
        super(firstName, lastName, department, role);
        this.email = "NOT_IMPLEMENTED";
    }

    /**
     * Print the name of the manager
     *
     * @return the complete name of the manager
     * @see String
     */
    public String toString() {
        return getFirstName() + " " + getLastName();
    }

}
