package com.burnier.timeclockproject.company;

import java.io.Serializable;

public enum Role implements Serializable {
    LEADING,
    NOT_LEADING,
    NOT_A_MANAGER
}
