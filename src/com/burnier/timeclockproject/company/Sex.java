package com.burnier.timeclockproject.company;

import java.io.Serializable;

public enum Sex implements Serializable {
    MALE,
    FEMALE,
    UNKNOWN
}


