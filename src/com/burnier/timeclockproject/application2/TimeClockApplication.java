package com.burnier.timeclockproject.application2;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import jfxtras.styles.jmetro.JMetro;
import jfxtras.styles.jmetro.Style;

import java.util.Objects;

public class TimeClockApplication extends Application {
    /**
     * Set the Stage
     * @param primaryStage the Stage
     * @throws Exception an Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("timeclock.fxml")));
        JMetro jMetro = new JMetro(Style.LIGHT);
        primaryStage.setTitle("Timeclock emulator - V1");
        primaryStage.getIcons().add(new Image("file:timeclock_icon.png"));
        Scene scene = new Scene(root, 500, 275);
        jMetro.setScene(scene);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setOnCloseRequest(windowEvent -> {
            Platform.exit();
            System.exit(0);
        });
    }

    /**
     * Launch the window of the application
     *
     * @param args the args
     */
    public static void main(String[] args) {
        launch(args);
    }
}
