package com.burnier.timeclockproject.application2;

import com.burnier.timeclockproject.Server.TCPClientFile;
import com.burnier.timeclockproject.application1.ControllerTableHandler;
import com.burnier.timeclockproject.company.Company;
import com.burnier.timeclockproject.file.Deserialization;
import com.burnier.timeclockproject.file.PropertiesReader;
import com.burnier.timeclockproject.utils.RoundTimeRunner;
import com.burnier.timeclockproject.utils.Time;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Paint;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.Locale;
import java.util.ResourceBundle;

import static java.time.format.FormatStyle.LONG;

public class ControllerTimeClock implements Initializable {
    @FXML
    private javafx.scene.shape.Circle Circle;
    @FXML
    private Label labelTime2;
    @FXML
    private Label dateLabel;
    @FXML
    private Button buttonCheck;
    @FXML
    private Label timeLabel;
    @FXML
    private ChoiceBox<String> list;
    private final DropShadow shadow = new DropShadow();
    private LinkedList<String> listOfEmployeeName = new LinkedList<>();
    private LinkedList<String> temp = new LinkedList<>();
    private final LinkedList<String> listOfEmployeeDateAndId = new LinkedList<>();
    private final LinkedList<String> storage = new LinkedList<>();
    private static Company company = new Company();
    private boolean connected;
    private int port;

    /**
     * Set the list of check in or check out which will be sent
     */
    public void sendCheck() {

       /* final java.net.URL resource = getClass().getResource("mix.mp3");
        final Media media = new Media(resource.toString());
        final MediaPlayer mediaPlayer = new MediaPlayer(media);
        mediaPlayer.play();*/

        if (list.getSelectionModel().getSelectedItem() != null) {
            for (String s : listOfEmployeeName) {
                if (s.split("id: ")[1].equals(list.getSelectionModel().getSelectedItem().split("id: ")[1])) {
                    listOfEmployeeDateAndId.clear();
                    listOfEmployeeDateAndId.add(s.split("id: ")[1] + " " + LocalTime.now() + " " + LocalDate.now());
                }
            }
            if (connected) {
                storage.add(listOfEmployeeDateAndId.getLast());
            } else {
                storage.clear();
            }
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText("TimeClock Information");
            if(connected){
                alert.setContentText("Check-in / Check-out added.");
            }else{
                alert.setContentText("Check-in / Check-out will be added when reconnected to dashboard.");
            }
            alert.showAndWait();
            list.getSelectionModel().clearSelection();
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText("Problem with a selection");
            alert.setContentText("Empty selection");
            alert.showAndWait();
        }
    }

    /**
     * Set the shadow effect of the button check
     */
    public void MouseEntered() {
        buttonCheck.setEffect(shadow);
    }

    /**
     * Remove the shadow effect of the button check
     */
    public void MouseExit() {
        buttonCheck.setEffect(null);
    }

    /**
     * Set the controller of the time clock
     *
     * @param url            the url
     * @param resourceBundle the bundle of resources
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            Deserialization deserialization = new Deserialization();
            Thread deserializationThread = new Thread(deserialization);
            deserializationThread.start();
            deserializationThread.join();
            if (deserialization.getCompany() != null) {
                company = deserialization.getCompany();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        initializeClock();

        setListOfEmployee();
        temp = listOfEmployeeName;
        ObservableList<String> items = FXCollections.observableArrayList(listOfEmployeeName);
        list.setItems(items);

        //TODO UPDATE THIS PART
        new Thread(() -> {
           while (true) {
                try {
                    Thread propertiesThread = new Thread(new PropertiesReader(port));
                    TCPClientFile tcpClientFile = new TCPClientFile(listOfEmployeeDateAndId, port);
                    Thread tClient = new Thread(tcpClientFile);
                    tClient.start();
                    propertiesThread.start();
                    propertiesThread.join();
                    tClient.join();

                    if (tcpClientFile.getEmployee().size() != 0) {
                        temp = listOfEmployeeName;
                        listOfEmployeeName = tcpClientFile.getEmployee();
                        connected = true;
                    } else {
                        setListOfEmployeeDateAndId(storage);
                        listOfEmployeeName = temp;
                        connected = false;
                    }
                    if (PropertiesReader.getPort() == 0) {
                        port = 8080;
                    } else {
                        port = PropertiesReader.getPort();
                    }

                    if (connected) {
                        buttonCheck.setStyle("-fx-background-color: limegreen");
                    } else {
                        buttonCheck.setTextFill(Paint.valueOf("white"));
                        buttonCheck.setStyle("-fx-background-color: orangered");

                    }
                    if (listOfEmployeeName.size() != temp.size()) {
                        Platform.runLater(() -> {
                            items.clear();
                            items.addAll(listOfEmployeeName);
                            list.setItems(items);
                        });
                    }
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /**
     * Get the company
     *
     * @return the company from the thread
     * @see Company
     */
    public static Company getCompany() {
        return company;
    }

    /**
     * Set the list of employee for the choice box
     */
    private void setListOfEmployee() {
        ControllerTableHandler.setListOfEmployeeNameAndId(company, listOfEmployeeName);
    }

    /**
     * Set the list of check in and check out which will be sent
     *
     * @param listOfEmployeeCheck the list of employee's check
     */
    private void setListOfEmployeeDateAndId(LinkedList<String> listOfEmployeeCheck) {
        listOfEmployeeDateAndId.clear();
        listOfEmployeeDateAndId.addAll(listOfEmployeeCheck);
    }

    /**
     * Initialize the time clock
     */
    private void initializeClock(){
        new Thread(new Time(timeLabel)).start();
        new Thread(new RoundTimeRunner(labelTime2)).start();
        DateTimeFormatter formatter2 = DateTimeFormatter.ofLocalizedDate(LONG);
        formatter2 = formatter2.withLocale(Locale.US);
        dateLabel.setText(LocalDate.now().format(formatter2));
        dateLabel.setAlignment(Pos.CENTER);
    }
}
