package com.burnier.timeclockproject.application1;

import com.burnier.timeclockproject.Server.TCPServerFile;
import com.burnier.timeclockproject.company.*;
import com.burnier.timeclockproject.file.*;
import com.burnier.timeclockproject.utils.WorkingDay;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import javafx.util.converter.LocalTimeStringConverter;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.ResourceBundle;

import static com.burnier.timeclockproject.company.Role.*;
import static com.burnier.timeclockproject.company.Sex.*;

public class ControllerTableHandler implements Initializable {
    @FXML
    private Button exportChecking;
    @FXML
    private Label ipAddressLabel;
    @FXML
    private Label portLabel;
    @FXML
    private ChoiceBox<String> choiceBoxDepartment;
    @FXML
    private Button ImportEmployee;
    @FXML
    private Button removeEmployee;
    @FXML
    private Button ExportEmployee;
    @FXML
    private Button buttonStatut;
    @FXML
    private Button ButtonAddDepartment;
    @FXML
    private Button ButtonRemoveDepartment;
    @FXML
    private Button ButtonAddManager;
    @FXML
    private Button buttonAddEmployee;
    @FXML
    private Button buttonIp;
    @FXML
    private Button buttonPort;
    @FXML
    private Button importChecking;
    @FXML
    private Button buttonReset;
    @FXML
    private TextField fieldPathChecking;
    @FXML
    private RadioButton level1;
    @FXML
    private RadioButton level2;
    @FXML
    private RadioButton level3;
    @FXML
    private RadioButton level4;
    @FXML
    private RadioButton level5;
    @FXML
    private TextField textIp;
    @FXML
    private TextField textPort;
    @FXML
    private DatePicker dateSearch;
    @FXML
    private TableColumn<Employee, Role> roleColumn;
    @FXML
    private TableColumn<Employee, String> checkOutDate;
    @FXML
    private TableColumn<Department, String> departmentTextName;
    @FXML
    private ChoiceBox<Role> choiceBoxRolePromote;
    @FXML
    private TextField textDepartmentName;
    @FXML
    private ChoiceBox<String> buttonListEmployee;
    @FXML
    private TableView<Employee> tableEmployee;
    @FXML
    private TableColumn<Department, Manager> managerLeading;
    @FXML
    private TableColumn<Department, Integer> numberOfEmployee;
    @FXML
    private ChoiceBox<Role> choiceBoxRole;
    @FXML
    private TableColumn<Employee, String> firstName;
    @FXML
    private TableColumn<Employee, String> lastName;
    @FXML
    private TableColumn<Employee, Integer> age;
    @FXML
    private TableColumn<Employee, Sex> gender;
    @FXML
    private TableColumn<Employee, String> address;
    @FXML
    private TableColumn<Employee, LocalTime> timeA;
    @FXML
    private TableColumn<Employee, LocalTime> timeE;
    @FXML
    private TableColumn<Employee, Integer> tableID;
    @FXML
    private TableView<Department> tableDepartment;
    @FXML
    private TableView<Employee> tableCheck;
    @FXML
    private TableColumn<Employee, Integer> tableIdCheck;
    @FXML
    private TableColumn<Employee, String> checkIn;
    @FXML
    private TableColumn<Employee, String> checkOut;
    @FXML
    private TableColumn<Employee, String> offSet;
    @FXML
    private TableColumn<Employee, String> checkDate;
    @FXML
    private TableColumn<Employee, String> nameDepartment;
    @FXML
    private TextField textImport;
    @FXML
    private TextField textFirstName;
    @FXML
    private TextField textLastName;
    @FXML
    private TextField textDepartment;
    @FXML
    private ChoiceBox<String> listDepartmentChoice;
    @FXML
    private ChoiceBox<String> listEmployeeChoice;

    private final ToggleGroup group = new ToggleGroup();
    private static Company company = new Company();
    private final LinkedList<String> listOfEmployeeName = new LinkedList<>();
    private final LinkedList<String> listOfEmployeeNameAndId = new LinkedList<>();
    private LinkedList<String> listOfEmployeeDateAndId = new LinkedList<>();
    private final ObservableList<Department> departmentObservableList = FXCollections.observableArrayList();
    private final ObservableList<String> nameAndIdObservableList = FXCollections.observableArrayList();
    private final ObservableList<Employee> observableListEmployeeTable = FXCollections.observableArrayList();
    private final ObservableList<String> nameEmployeeObservableList = FXCollections.observableArrayList();
    private final ObservableList<String> nameDepartmentObservableList = FXCollections.observableArrayList();
    private final ObservableList<Employee> filterObservableList = FXCollections.observableArrayList();
    private final ObservableList<Employee> dataEmployeeCheck = FXCollections.observableArrayList();

    private int port;
    private String ip;
    private final TCPServerFile tcpServerFile = new TCPServerFile(listOfEmployeeNameAndId, port); //Listen all the modification
    private Thread tServer;

    /**
     * Set the list of employee
     */
    private void setListOfEmployee(Company company) {
        listOfEmployeeName.clear();
        for (Department department : company.getListOfDepartment()) {
            for (Employee employee : department.getListEmployee()) {
                listOfEmployeeName.add(employee.getName());
            }
        }
    }

    /**
     * Set the list of department
     */
    private void refreshListObservableDepartment(Company company) {
        nameDepartmentObservableList.clear();
        departmentObservableList.clear();
        departmentObservableList.addAll(company.getListOfDepartment());
        for (Department department : company.getListOfDepartment()) {
            nameDepartmentObservableList.add(department.getNameDepartment());
        }
        listDepartmentChoice.setItems(nameDepartmentObservableList);
        choiceBoxDepartment.setItems(nameDepartmentObservableList);
        tableDepartment.setItems(departmentObservableList);
    }

    /**
     * Set the list of department
     */
    private void refreshListObservableEmployee(Company company) {
        observableListEmployeeTable.clear();
        nameEmployeeObservableList.clear();
        nameAndIdObservableList.clear();
        for (Department department : company.getListOfDepartment()) {
            observableListEmployeeTable.addAll(department.getListEmployee());
            for (Employee employee : department.getListEmployee()) {
                nameEmployeeObservableList.add(employee.getName());
                nameAndIdObservableList.add(employee.getId() + " - " + employee.getName());
            }
        }

        buttonListEmployee.setItems(nameAndIdObservableList);
        listEmployeeChoice.setItems(nameEmployeeObservableList);
        tableEmployee.setItems(observableListEmployeeTable);
    }

    /**
     * Set the list of name and id from employee
     */
    private void setListOfEmployeeNameAndId() throws InterruptedException {
        Thread.sleep(500);
        setListOfEmployeeNameAndId(company, listOfEmployeeNameAndId);
    }

    public static void setListOfEmployeeNameAndId(Company company, LinkedList<String> listOfEmployeeNameAndId) {
        listOfEmployeeNameAndId.clear();
        for (Department department : company.getListOfDepartment()) {
            for (Employee employee : department.getListEmployee()) {
                listOfEmployeeNameAndId.add(employee.getName() + " id: " + employee.getId());
            }
        }
    }

    /**
     * Set the controller
     *
     * @param url            the url
     * @param resourceBundle the resource bundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if (PropertiesReader.getIp() == null) {
            ip = "localhost";
        } else {
            ip = PropertiesReader.getIp();
        }
        if (PropertiesReader.getPort() == 0) {
            port = 8080;
        } else {
            port = PropertiesReader.getPort();
        }
        tcpServerFile.setPort(port);
        tcpServerFile.setIp(ip);
        try {
            Thread propertiesThread = new Thread(new PropertiesReader(port, ip));
            ipAddressLabel.setText(ipAddressLabel.getText().replace("IP", ip));
            portLabel.setText(portLabel.getText().replace("PORT", String.valueOf(port)));
            propertiesThread.start();
            propertiesThread.join();
            Deserialization deserialization = new Deserialization();
            Thread deserializationThread = new Thread(deserialization);
            deserializationThread.start();
            deserializationThread.join();
            if (deserialization.getCompany() != null) {
                company = deserialization.getCompany();
            }
            setListOfEmployeeNameAndId();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        tServer = new Thread(tcpServerFile);
        tServer.start();
        new Thread(() -> {
            while (true) {
                if (!tcpServerFile.getListOfCheck().isEmpty()) {
                    listOfEmployeeDateAndId = tcpServerFile.getListOfCheck();
                    setWorkingDayEmployee(listOfEmployeeDateAndId);
                    Platform.runLater(() -> refreshListEmployeeCheck(company));
                }
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        setToggleGroup();
        setListOfEmployee(company);
        refreshListObservableDepartment(company);
        initializeItems(company);

    }

    private void refreshListEmployeeCheck(Company company) {
        dataEmployeeCheck.clear();
        for (Department department : company.getListOfDepartment()) {
            dataEmployeeCheck.addAll(department.getListEmployee());
        }
        tableCheck.setItems(dataEmployeeCheck);
    }

    private void initializeItems(Company company) {
        choiceBoxRolePromote.setItems(FXCollections.observableArrayList(LEADING, NOT_LEADING, NOT_A_MANAGER));
        choiceBoxRole.setItems(FXCollections.observableArrayList(LEADING, NOT_LEADING, NOT_A_MANAGER));
        choiceBoxRole.getSelectionModel().select(NOT_A_MANAGER);
        refreshListObservableDepartment(company);
        refreshListObservableEmployee(company);
        tableEmployee.setEditable(true);
        tableDepartment.setEditable(true);

        initializeTableEmployee();

    }

    private void setToggleGroup() {
        level1.setToggleGroup(group);
        level2.setToggleGroup(group);
        level3.setToggleGroup(group);
        level4.setToggleGroup(group);
        level5.setToggleGroup(group);
    }

    private void initializeTableEmployee() {

        listDepartmentChoice.getSelectionModel().selectedIndexProperty().addListener((observableValue, name, name2) -> {
            filterObservableList.clear();
            for (Employee employee : dataEmployeeCheck) {
                if (listDepartmentChoice.getItems().get((Integer) name2).equals(employee.getNameDepartment())) {
                    filterObservableList.add(employee);
                }
            }
            tableCheck.setItems(filterObservableList);
            tableCheck.refresh();
        });
        listEmployeeChoice.getSelectionModel().selectedIndexProperty().addListener((observableValue, name, name2) -> {
            filterObservableList.clear();
            for (Employee employee : dataEmployeeCheck) {
                if (listEmployeeChoice.getItems().get((Integer) name2).equals(employee.getName())) {
                    filterObservableList.add(employee);
                }
            }
            tableCheck.setItems(filterObservableList);
            tableCheck.refresh();
        });

        firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        gender.setCellValueFactory(new PropertyValueFactory<>("gender"));
        address.setCellValueFactory(new PropertyValueFactory<>("address"));
        address.setCellFactory(TextFieldTableCell.forTableColumn());
        age.setCellValueFactory(new PropertyValueFactory<>("age"));
        age.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        tableID.setCellValueFactory(new PropertyValueFactory<>("id"));
        timeA.setCellValueFactory(new PropertyValueFactory<>("timeArrival"));
        timeA.setCellFactory(TextFieldTableCell.forTableColumn(new LocalTimeStringConverter()));
        timeE.setCellValueFactory(new PropertyValueFactory<>("timeEnding"));
        timeE.setCellFactory(TextFieldTableCell.forTableColumn(new LocalTimeStringConverter()));
        roleColumn.setCellValueFactory(new PropertyValueFactory<>("role"));
        tableIdCheck.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableCheck.setEditable(true);
        tableIdCheck.setCellValueFactory(new PropertyValueFactory<>("id"));
        managerLeading.setCellValueFactory(new PropertyValueFactory<>("manager"));
        managerLeading.setCellFactory(column -> new TableCell<>() {
            @Override
            protected void updateItem(Manager manager, boolean empty) {

                super.updateItem(manager, empty);
                if (empty) {
                    setText(null);
                } else if (manager == null) {
                    setText(" NO MANAGER LEADING ");
                    setTextFill(Paint.valueOf("white"));
                    setStyle("-fx-background-color: firebrick");
                } else {
                    setText(manager.toString());
                    setTextFill(Paint.valueOf("white"));
                    setStyle("-fx-background-color: forestgreen");

                }
            }

        });
        checkIn.setCellValueFactory(data -> {
            if (data.getValue().getWorkingDay().size() == 0) {
                return new SimpleStringProperty("null");
            } else {
                return new SimpleStringProperty(data.getValue().getWorkingDay().getFirst().getTimeCheckIn() + " ");
            }
        });
        checkOut.setCellValueFactory(data -> {
            if (data.getValue().getWorkingDay().size() == 0) {
                return new SimpleStringProperty("null");
            } else {
                return new SimpleStringProperty(data.getValue().getWorkingDay().getFirst().getTimeCheckOut() + " ");
            }
        });
        offSet.setCellValueFactory(data -> {
            if (data.getValue().getWorkingDay().size() == 0) {
                return new SimpleStringProperty("0");
            } else {
                return new SimpleStringProperty(String.valueOf(data.getValue().getWorkingDay().getFirst().getTotalOffSet()));
            }
        });
        checkDate.setCellValueFactory(data -> {
            if (data.getValue().getWorkingDay().size() == 0) {
                return new SimpleStringProperty("null");
            } else {
                return new SimpleStringProperty(data.getValue().getWorkingDay().getFirst().getCheckInDate() + " ");
            }
        });
        checkOutDate.setCellValueFactory(data -> {
            if (data.getValue().getWorkingDay().size() == 0) {
                return new SimpleStringProperty("null");
            } else {
                return new SimpleStringProperty(data.getValue().getWorkingDay().getFirst().getCheckOutDate() + " ");
            }
        });
        nameDepartment.setCellValueFactory(data -> {
            if (data.getValue().getDepartment() == null) {

                return new SimpleStringProperty("null");
            } else {
                return new SimpleStringProperty(data.getValue().getNameDepartment());
            }
        });
        departmentTextName.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getNameDepartment()));
        numberOfEmployee.setCellValueFactory(data -> new SimpleObjectProperty<>(data.getValue().getListEmployee().size()));

        nameDepartment.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<>() {

            @Override
            public String toString(String s) {
                return s;
            }

            @Override
            public String fromString(String s) { //TODO REFACTOR
                try {
                    for (Department department : company.getListOfDepartment()) {
                        for (Employee employee : department.getListEmployee()) {
                            if (tableEmployee.getSelectionModel().getSelectedItem().equals(employee)) {
                                if (employee.getRole().equals(LEADING)) {
                                    createAndShowModal(Alert.AlertType.WARNING, "Warning Dialog", "Manager", "Remove the leading role before changing the department");
                                    return employee.getDepartment().getNameDepartment();
                                } else {
                                    department.getListEmployee().remove(employee);

                                    Department departmentS = company.searchDepartment(s);
                                    employee.setDepartment(departmentS);
                                    departmentS.getListEmployee().add(employee);

                                    return departmentS.getNameDepartment();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tableDepartment.refresh();
                return s;
            }
        }));
        gender.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<Sex>() {
            @Override
            public String toString(Sex sex) {
                return parseEnum(sex);
            }

            @Override
            public Sex fromString(String s) {
                for (Department department : company.getListOfDepartment()) {
                    for (Employee employee : department.getListEmployee()) {
                        if (employee.equals(tableEmployee.getSelectionModel().getSelectedItem())) {
                            employee.setGender(parseEnum(s));
                        }
                    }
                }
                refreshListObservableEmployee(company);
                return parseEnum(s);
            }
        }));
    }

    /**
     * Parse a Sex object as a Sex string
     *
     * @param sex the Sex object
     * @return the sex as a string
     * @see String
     */
    private static String parseEnum(Sex sex) {
        if (sex == MALE) {
            return "MALE";
        } else if (sex == FEMALE) {
            return "FEMALE";
        } else {
            return "UNKNOWN";
        }
    }

    /**
     * Parse the string sex as a Sex object
     *
     * @param stringSex the Sex as a string
     * @return the Sex object
     * @see Sex
     */
    private static Sex parseEnum(String stringSex) {
        if (stringSex.equals("MALE")) {
            return MALE;
        } else if (stringSex.equals("FEMALE")) {
            return FEMALE;
        } else {
            return UNKNOWN;
        }
    }

    /**
     * Allow to remove an employee in the tableView
     */
    public void ActionRemoveEmployee() throws InterruptedException { //TODO REFACTOR
        if (company.getListOfDepartment() != null)
            if (!company.getListOfDepartment().getFirst().getListEmployee().isEmpty()) {
                for (Department department : company.getListOfDepartment()) {
                    for (Employee employee : department.getListEmployee()) {
                        if (employee.equals(tableEmployee.getSelectionModel().getSelectedItem())) {
                            if (tableEmployee.getSelectionModel().getSelectedItem().getRole().equals(LEADING)) {
                                createAndShowModal(Alert.AlertType.WARNING, "Warning Dialog", "Manager", "Please change leading status before removing");
                            } else {
                                employee.getDepartment().getListEmployee().remove(employee);
                            }
                        }
                    }
                }
                refreshListObservableEmployee(company);
                refreshListObservableDepartment(company);
                tableCheck.refresh();
                tableDepartment.refresh();
                tableEmployee.refresh();

                //TimeClock update
                setListOfEmployeeNameAndId();
                tcpServerFile.setListOfEmployee(listOfEmployeeNameAndId);
            } else {
                createAndShowModal(Alert.AlertType.ERROR, "Error Dialog", "Employee", "No employee selected");
            }
    }

    /**
     * Allow to import the employee from a file
     */
    public void ActionImport() { //TODO REFACTOR
        //TODO DISPLAY WINDOWS DIRECTORY
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select the file to import");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        File file = fileChooser.showOpenDialog(new Stage());
        if (file != null) { //TODO Check if file exist
            CsvFileReader.readCsvFile(file.getAbsolutePath(), company);
        }
        refreshListObservableDepartment(company);
        refreshListObservableEmployee(company);
    }

    /**
     * Allow to export the list of employee contains in the tableView
     */
    public void ActionExport() throws IOException {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Select the directory to export the file");
        directoryChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        File dir = directoryChooser.showDialog(new Stage());
        if (dir != null) { //TODO Check if directory exist
            CsvFileWriter.writeCsvFile(dir.getAbsolutePath(), "employee_export.txt", company);
        }

    }

    /**
     * Allow to remove the department in the tableView
     */
    public void removeDepartment() {
        if (tableDepartment.getSelectionModel().getSelectedItem() == null) {
            createAndShowModal(Alert.AlertType.ERROR, "Error Dialog", "Department", "No department selected");
        } else {
            company.getListOfDepartment().remove(tableDepartment.getSelectionModel().getSelectedItem());
            int index = 0;
            for (Department department : company.getListOfDepartment()) {
                if (department.getNameDepartment().contains("Department name")) {
                    index++;
                    department.setNameDepartment(department.getNameDepartment().replace(department.getNameDepartment().substring((department.getNameDepartment().indexOf("°") + 1)), String.valueOf(index)));
                }
            }
            refreshListObservableDepartment(company);
            refreshListObservableEmployee(company);
            tableCheck.refresh();
            tableDepartment.refresh();
            tableEmployee.refresh();
        }
    }

    /**
     * Allow to add an employee in the tableView
     *
     * @throws Exception
     */
    public void addEmployeeOrManager() throws Exception {
        int count = 1;
        int k = 0;
        ArrayList<Integer> listOfId = new ArrayList<>();
        if (textFirstName.getText().equals("") || textLastName.getText().equals("") || company.searchDepartment(choiceBoxDepartment.getSelectionModel().getSelectedItem()) == null) {
            createAndShowModal(Alert.AlertType.ERROR, "Error Dialog", "Employee", "Information missing");
        } else {
            Department department = company.searchDepartment(choiceBoxDepartment.getSelectionModel().getSelectedItem());
            if (choiceBoxRole.getSelectionModel().getSelectedItem().equals(LEADING) || choiceBoxRole.getSelectionModel().getSelectedItem().equals(NOT_LEADING)) {
                if (choiceBoxRole.getValue().equals(LEADING) && department.getManager() != null) {
                    createAndShowModal(Alert.AlertType.WARNING, "Warning Dialog", "Manager", "Already a manager leading the department");
                } else {
                    if (choiceBoxRole.getSelectionModel().getSelectedItem().equals(NOT_LEADING)) {
                        Manager manager = new Manager(textFirstName.getText(), textLastName.getText(), choiceBoxRole.getValue(), department);
                        setIdEmployee(count, k, listOfId, manager);
                        department.getListEmployee().add(manager); //TODO CREATE LIST MANAGER INSTEAD
                    } else {
                        Manager manager = new Manager(textFirstName.getText(), textLastName.getText(), choiceBoxRole.getValue(), department);
                        setIdEmployee(count, k, listOfId, manager);
                        department.addManager(manager, company);
                        createAndShowModal(Alert.AlertType.INFORMATION, "Information Dialog", "Manager", "Manager successfully added");
                    }
                }
            } else {
                Employee employee = new Employee(textFirstName.getText(), textLastName.getText(), department, choiceBoxRole.getSelectionModel().getSelectedItem());
                setIdEmployee(count, k, listOfId, employee); //TODO REFACTOR
                department.addEmployee(employee);
                createAndShowModal(Alert.AlertType.INFORMATION, "Information Dialog", "Employee", "Employee successfully added");
            }
            setListOfEmployee(company);
            refreshListObservableEmployee(company);
            tableCheck.refresh();
            tableDepartment.refresh();
            tableEmployee.refresh();
            textFirstName.clear();
            textLastName.clear();
            choiceBoxDepartment.getSelectionModel().clearSelection();

            //TimeClock update
            setListOfEmployeeNameAndId();
            tcpServerFile.setListOfEmployee(listOfEmployeeNameAndId);
        }
    }

    /**
     * Set the id of the employee's list
     *
     * @param count    Used to set the id
     * @param k
     * @param listOfId the list of id
     * @param employee the employee
     */
    private void setIdEmployee(int count, int k, ArrayList<Integer> listOfId, Employee employee) {
        boolean verify = false;
        if (company.getListOfDepartment().getFirst().getListEmployee().size() != 0) {

            for (Department department : company.getListOfDepartment()) {
                for (Employee employeeA : department.getListEmployee()) {
                    listOfId.add(employeeA.getId());
                }
            }

            Collections.sort(listOfId);
            while (k < listOfId.size() && !verify) {
                if (listOfId.get(k) == count) {
                    count++;
                } else {
                    verify = true;
                }
                k++;
            }
            employee.setId(count);
        } else {
            employee.setId(1);
        }
    }

    /**
     * Allow to add a department in a tableView
     *
     * @throws Exception
     */
    public void ActionAddDepartment() throws Exception {
        Department department = new Department("Department name n°" + (company.getListOfDepartment().size() + 1));
        company.addDepartment(department);

        refreshListObservableDepartment(company);

        tableDepartment.refresh();
    }

    /**
     * Allow to promote or demote an employee
     *
     */
    public void ActionPromote() {
        if (buttonListEmployee.getValue() == null || choiceBoxRolePromote.getValue() == null) {
            createAndShowModal(Alert.AlertType.ERROR, "Error Dialog", "Employee", "No employee or role selected");
        } else {
            for (Department department : company.getListOfDepartment()) {
                for (Employee employee : department.getListEmployee()) {
                    if (employee.getId() == Integer.parseInt(buttonListEmployee.getSelectionModel().getSelectedItem().substring(0, buttonListEmployee.getSelectionModel().getSelectedItem().indexOf("-") - 1))) {
                        if (!choiceBoxRolePromote.getValue().equals(employee.getRole())) {
                            if (choiceBoxRolePromote.getValue().equals(LEADING)) {
                                if (department.getManager() != null) {
                                    createAndShowModal(Alert.AlertType.ERROR, "Error Dialog", "Manager", "Already a manager leading the department.");
                                } else {
                                    Manager manager = new Manager(employee, LEADING);
                                    department.getListEmployee().remove(employee);
                                    department.getListEmployee().add(manager);
                                    department.setManager(manager);
                                }
                            } else if ((choiceBoxRolePromote.getValue().equals(NOT_LEADING) || choiceBoxRolePromote.getValue().equals(NOT_A_MANAGER)) && employee.getRole().equals(LEADING)) {
                                employee.setRole(choiceBoxRolePromote.getValue());
                                department.setManager(null);
                            } else if (choiceBoxRolePromote.getValue().equals(NOT_LEADING)) {
                                Manager manager = new Manager(employee, NOT_LEADING);
                                department.getListEmployee().remove(employee);
                                department.getListEmployee().add(manager);
                            } else {
                                employee.setRole(NOT_LEADING);
                            }
                        } else {
                            createAndShowModal(Alert.AlertType.ERROR, "Error Dialog", "Employee", "Already that  role.");
                        }
                        break;
                    }
                }
            }
            refreshListObservableDepartment(company);
            refreshListObservableEmployee(company);
            tableEmployee.refresh();
            tableDepartment.refresh();
        }
    }

    /**
     * Get the company
     *
     * @return the company
     * @see Company
     */
    public static Company getCompany() {
        return company;
    }

    /**
     * Set the time ending when committed in the tableView
     *
     * @param employeeLocalTimeCellEditEvent the cell of the tableColumn
     */
    public void TimeECommit(CellEditEvent<Employee, LocalTime> employeeLocalTimeCellEditEvent) {
        for (Department department : company.getListOfDepartment()) {
            for (Employee employee : department.getListEmployee()) {
                if (employee.getId() == employeeLocalTimeCellEditEvent.getRowValue().getId())
                    employee.setTimeEnding(employeeLocalTimeCellEditEvent.getNewValue());
            }

        }
    }

    /**
     * Allow to reset the filtered data
     */
    public void ActionReset() {
        tableCheck.setItems(dataEmployeeCheck);
        tableCheck.refresh();
        listEmployeeChoice.getSelectionModel().clearSelection();
        listDepartmentChoice.getSelectionModel().clearSelection();
        dateSearch.setValue(null);
    }

    /**
     * Allow to search the check in or check out with date
     */
    public void ActionSearchDate() {
        if (dateSearch.getValue() != null) {
            filterObservableList.clear();
            for (Employee employee : dataEmployeeCheck) {
                for (WorkingDay workingDay : employee.getWorkingDay()) {
                    if (dateSearch.getValue().equals(workingDay.getCheckInDate())) {
                        filterObservableList.add(employee);
                    }
                    if (dateSearch.getValue().equals(workingDay.getCheckOutDate())) {
                        filterObservableList.add(employee);
                    }
                }
            }
            tableCheck.setItems(filterObservableList);
            tableCheck.refresh();
            dateSearch.setValue(null);
        }
    }

    /**
     * Set the time arrival when committed in the tableView
     *
     * @param employeeLocalTimeCellEditEvent the cell of the tableColumn
     */
    public void ActionEditA(CellEditEvent<Employee, LocalTime> employeeLocalTimeCellEditEvent) {
        for (Department department : company.getListOfDepartment()) {
            for (Employee employee : department.getListEmployee()) {
                if (employee.getId() == employeeLocalTimeCellEditEvent.getRowValue().getId())
                    employee.setTimeArrival(employeeLocalTimeCellEditEvent.getNewValue());
            }

        }
        tableEmployee.refresh();
    }

    /**
     * Set the ip
     */
    public void ActionGetIp() throws InterruptedException {
        tServer.interrupt();
        Thread tWrite = new Thread(new PropertiesWriter(port, textIp.getText()));
        tWrite.start();
        tWrite.join();
        tcpServerFile.setIp(textIp.getText());
        tServer = new Thread(tcpServerFile);
        tServer.start();
        textIp.clear();
    }

    /**
     * Set the port
     */
    public void ActionGetPort() throws InterruptedException {
        tServer.interrupt();
        Thread tWrite = new Thread(new PropertiesWriter(Integer.parseInt(textPort.getText()), ip)); //TODO CHECK PORT IF NUMBER
        tWrite.start();
        tWrite.join();
        port = Integer.parseInt(textPort.getText());
        tcpServerFile.setPort(port);
        tServer = new Thread(tcpServerFile);
        tServer.start();
        textPort.clear();
    }

    /**
     * Color a specific offset
     */
    public void ActionLevel1() {
        offSet.setCellFactory(column -> new TableCell<>() {
            @Override
            protected void updateItem(String offSet, boolean empty) {

                super.updateItem(offSet, empty);
                if (empty) {
                    setText(null);
                } else if (Integer.parseInt(offSet) == -15) {
                    setText(offSet);
                    setStyle("-fx-background-color: deepskyblue");
                } else if (Integer.parseInt(offSet) == 15) {
                    setText(offSet);
                    setStyle("-fx-background-color: deepskyblue");
                } else {
                    setText(offSet);
                }

            }
        });
    }

    /**
     * Import the checking of the employee
     */
    public void ActionImportChecking() {
        if (fieldPathChecking.getText().equals("")) {
            createAndShowModal(Alert.AlertType.ERROR, "Error Dialog", "File import", "File path missing");
        } else {
            CsvFileReaderChecking.readCsvFileChecking(fieldPathChecking.getText(), company);
        }
        fieldPathChecking.clear();
    }

    /**
     * Set the working day of the employee
     *
     * @param listOfEmployeeDateAndId the list of working days
     */
    private void setWorkingDayEmployee(LinkedList<String> listOfEmployeeDateAndId) {

        for (String s : listOfEmployeeDateAndId) {
            for (Department department : company.getListOfDepartment()) {
                for (Employee employee : department.getListEmployee()) {
                    if (employee.getId() == Integer.parseInt(s.substring(0, s.indexOf(" ")))) {
                        try {
                            company.getCheckInCheckOut().checkInOut(employee, s);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            tableCheck.refresh();
        }
    }

    /**
     * Allow to color a specific offset
     */
    public void ActionLevel2() {
        offSet.setCellFactory(column -> new TableCell<>() {
            @Override
            protected void updateItem(String offSet, boolean empty) {

                super.updateItem(offSet, empty);
                if (empty) {
                    setText(null);
                } else if (Integer.parseInt(offSet) == -30) {
                    setText(offSet);
                    setStyle("-fx-background-color: dodgerblue");
                } else if (Integer.parseInt(offSet) == 30) {
                    setText(offSet);
                    setStyle("-fx-background-color: dodgerblue");
                } else {
                    setText(offSet);
                }

            }
        });
    }

    /**
     * Allow to color a specific offset
     */
    public void ActionLevel3() {
        offSet.setCellFactory(column -> new TableCell<>() {
            @Override
            protected void updateItem(String offSet, boolean empty) {

                super.updateItem(offSet, empty);
                if (empty) {
                    setText(null);
                } else if (Integer.parseInt(offSet) == -45) {
                    setText(offSet);
                    setStyle("-fx-background-color: yellow");
                } else if (Integer.parseInt(offSet) == 45) {
                    setText(offSet);
                    setStyle("-fx-background-color: yellow");
                } else {
                    setText(offSet);
                }

            }
        });
    }

    /**
     * Allow to color a specific offset
     */
    public void ActionLevel4() {
        offSet.setCellFactory(column -> new TableCell<>() {
            @Override
            protected void updateItem(String offSet, boolean empty) {

                super.updateItem(offSet, empty);
                if (empty) {
                    setText(null);
                } else if (Integer.parseInt(offSet) == -60) {
                    setText(offSet);
                    setStyle("-fx-background-color: orangered");
                } else if (Integer.parseInt(offSet) == 60) {
                    setText(offSet);
                    setStyle("-fx-background-color: orangered");
                } else {
                    setText(offSet);
                }

            }
        });
    }

    /**
     * Allow to color a specific offset
     */
    public void ActionLevel5() {
        offSet.setCellFactory(column -> new TableCell<>() {
            @Override
            protected void updateItem(String offSet, boolean empty) {

                super.updateItem(offSet, empty);
                if (empty) {
                    setText(null);
                } else if (Integer.parseInt(offSet) < -60) {
                    setText(offSet);
                    setStyle("-fx-background-color: red");
                } else if (Integer.parseInt(offSet) > 60) {
                    setText(offSet);
                    setStyle("-fx-background-color: red");
                } else {
                    setText(offSet);
                }

            }
        });
    }

    /**
     * Allow to set the age of the employee
     *
     * @param employeeIntegerCellEditEvent the cell of the tableColumn
     */
    public void ActionSetAge(CellEditEvent<Employee, Integer> employeeIntegerCellEditEvent) {
        for (Department department : company.getListOfDepartment()) {
            for (Employee employee : department.getListEmployee()) {
                if (tableEmployee.getSelectionModel().getSelectedItem().equals(employee)) {
                    if (employeeIntegerCellEditEvent.getNewValue() < 0) {
                        createAndShowModal(Alert.AlertType.ERROR, "Error Dialog", "Employee", "The age must be above zero");
                    } else {
                        employee.setAge(employeeIntegerCellEditEvent.getNewValue());
                    }
                }
            }
        }
        tableEmployee.refresh();
    }

    /**
     * Set the address of the employee
     *
     * @param employeeStringCellEditEvent the cell of the tableColumn
     */
    public void ActionSetAddress(CellEditEvent<Employee, String> employeeStringCellEditEvent) {
        for (Department department : company.getListOfDepartment()) {
            for (Employee employee : department.getListEmployee()) {
                if (tableEmployee.getSelectionModel().getSelectedItem().equals(employee)) {
                    employee.setAddress(employeeStringCellEditEvent.getNewValue());
                }
            }
        }
        tableEmployee.refresh();
    }

    public static void createAndShowModal(Alert.AlertType alertType, String title, String header, String description) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(description);
        alert.showAndWait();
    }

    public void departmentNameCommit(CellEditEvent<Department, String> departmentStringCellEditEvent) {
        for (Department department : company.getListOfDepartment()) {
            for (Employee employee : department.getListEmployee()) {
                if (employee.getDepartment().getNameDepartment().equals(departmentStringCellEditEvent.getRowValue().getNameDepartment()))
                    employee.getDepartment().setNameDepartment(departmentStringCellEditEvent.getNewValue());
            }
        }
        for(Department department : company.getListOfDepartment()){
            if(department.getNameDepartment().equals(departmentStringCellEditEvent.getRowValue().getNameDepartment())){
                department.setNameDepartment(departmentStringCellEditEvent.getNewValue());
            }
        }
        tableDepartment.refresh();
    }

    public void ActionAddManager(MouseEvent mouseEvent) {
    }
}
