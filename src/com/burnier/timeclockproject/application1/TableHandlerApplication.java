package com.burnier.timeclockproject.application1;

import com.burnier.timeclockproject.company.Company;
import com.burnier.timeclockproject.file.Serialization;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import jfxtras.styles.jmetro.JMetro;
import jfxtras.styles.jmetro.Style;

import java.util.Objects;

public class TableHandlerApplication extends Application {
    /**
     * Set the stage
     *
     * @param primaryStage the primary Stage
     * @throws Exception an Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("table_handler.fxml")));
        JMetro jMetro = new JMetro(Style.LIGHT);
        primaryStage.setTitle("Parameter Interface");
        primaryStage.getIcons().add(new Image("file:table.png"));
        Scene scene = new Scene(root, 1000, 400);
        jMetro.setScene(scene);
        primaryStage.setScene(scene);
        primaryStage.isResizable();
        primaryStage.show();
        stageClosure(primaryStage);
    }

    /**
     * Allow to serialize when the window is closing
     *
     * @param primaryStage the primaryStage
     */
    public static void stageClosure(Stage primaryStage) {
        primaryStage.setOnCloseRequest(windowEvent -> {
            Company company = ControllerTableHandler.getCompany();
            try {
                Thread t1 = new Thread(new Serialization(company));
                t1.start();
                t1.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.exit();
            System.exit(0);
        });
    }

    /**
     * Launch the window of the application
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

}
