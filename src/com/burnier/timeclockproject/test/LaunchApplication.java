package com.burnier.timeclockproject.test;

import com.burnier.timeclockproject.application2.ControllerTimeClock;
import com.burnier.timeclockproject.company.Company;
import com.burnier.timeclockproject.file.Serialization;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class LaunchApplication extends Application implements Runnable {
    public static void main(String[] args) throws InterruptedException {
        new Thread(new LaunchApplication2()).start(); //TimeClock application
        Thread.sleep(2000);
        new Thread(new LaunchApplication()).start(); //Main application
    }

    @Override
    public void run() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    Stage anotherStage = new Stage();
                    Parent root2 = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("sample2.fxml")));

                    anotherStage.setTitle("Timeclock emulator - V1");
                    anotherStage.setScene(new Scene(root2, 500, 275));
                    anotherStage.show();
                    anotherStage.setOnCloseRequest(windowEvent -> {
                        try {
                        Company company = ControllerTimeClock.getCompany();
                        Thread t4 = new Thread(new Serialization(company));
                        t4.start();
                        t4.join();
                        Platform.exit();
                        System.exit(0);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void start(Stage stage) {
    }
}
