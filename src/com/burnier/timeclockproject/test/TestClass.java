package com.burnier.timeclockproject.test;
import com.burnier.timeclockproject.company.*;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;

/**
 * @author      Alexandre Burnier-Framboret
 * @version     1.0                (current version number of program)
 * @since       1.0          (the version of the package this class was first added to)
 */
public class TestClass {
    private Employee e1;
    private Manager e2;
    private Manager e3;
    private Department informatique;
    private Department technique;

    /**
     * Allow to Set up the test
     */
    @Before
    public void setUp() throws Exception {
        /*Company company = new Company(); //TOUT DOIT DEJA EXISTER AU NIVEAU DES DEPARTMENT POUR METTRE DES EMPLOYES AVEC CSV
        company.addDepartment(informatique);
        company.addDepartment(technique);
        informatique = new Department("informatique");
        technique = new Department("technique");
        e1 = new Employee("xxxxxxxx","xxxxxx", Sex.MALE,"xxxxxxxxxxxxxxx", 22, informatique, "10:15", "15:30");
        e2 = new Manager("xxxxxx","xxxxxxxx", Sex.MALE, Role.LEADING,"XXXXXXX", 22, informatique, "13:00", "17:45");
        e3 = new Manager("XXXX","xxxxxxxxxxxxx", Sex.MALE, Role.LEADING,"XXXXXXX", 22, informatique, "13:00", "17:45");

        informatique.addEmployee(e1);

        informatique.addManager(e2);

        company.getHistoryCompany();*/
    }
    /**
     * Allow to do the tests
     */
    @Test
    public void Test() throws Exception {
        /*String result1 = e1.getFirstName();
        assertEquals("xxxxxxxxxxxxx", result1);
        String result2 = e2.getAddress();
        assertEquals("XXXXXXX", result2);
        String result3 = e2.getNameDepartment();
        assertEquals("informatique", result3);
        String result4 = e1.getNameDepartment();
        assertEquals("informatique", result4);
        technique.addEmployee(e1);
        fail("Exception is waited with the adding of the same employee in both department");
        informatique.addManager(e3);
        fail("Exception is waited with the fact that only one manager can lead a department");*/
    }
}
