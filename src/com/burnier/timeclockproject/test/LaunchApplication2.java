package com.burnier.timeclockproject.test;

import com.burnier.timeclockproject.application1.TableHandlerApplication;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class LaunchApplication2 extends Application implements Runnable {

    @Override
    public void run() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    Stage primaryStage = new Stage();
                    Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("sample.fxml")));
                    primaryStage.setTitle("Parameter Interface");
                    primaryStage.setScene(new Scene(root, 605, 350));
                    primaryStage.isResizable();
                    primaryStage.show();
                    TableHandlerApplication.stageClosure(primaryStage);
                }catch (IOException e)
                {
                    System.out.println("Error");
                }
            }
        });
    }

    @Override
    public void start(Stage stage) {

    }
}
