package com.burnier.timeclockproject.utils;

import com.burnier.timeclockproject.company.Employee;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


public class CheckInCheckOut implements Serializable {
    private static final List<String> oldCheckInCheckOut = new ArrayList<>();
    private static final Logger LOGGER = Logger.getLogger(String.valueOf(CheckInCheckOut.class));

    /**
     * Allow to Check In/Out for the employee
     *
     * @param employee      the employee
     * @param employeeCheck the check in or check out from the time clock
     */
    public void checkInOut(Employee employee, String employeeCheck) {
        for (String check : oldCheckInCheckOut) {
            if (check.equals(employeeCheck)) {
                return;
            }
        }

        oldCheckInCheckOut.add(employeeCheck);

        final String hours = employeeCheck.substring(employeeCheck.indexOf(" ") + 1, employeeCheck.indexOf(":", 7));
        final String date = employeeCheck.substring(employeeCheck.indexOf(" ", 8) + 1);

        if (employee.getTimeArrival() != null && employee.getTimeEnding() != null) { //TODO FINISH THIS PART
            if (employee.getWorkingDay().size() != 0) {
                if (employee.getWorkingDay().getLast().getCheckInDate() != null) {
                    if (!employee.getWorkingDay().getLast().getCheckInDate().equals(LocalDate.now())) {
                        employee.getWorkingDay().add(new WorkingDay(RoundTime.roundToQuarter(LocalTime.parse(hours)), LocalDate.parse(date), WorkingDay.setOffSetTimeArrival(employee, LocalTime.parse(hours)), true));
                    }
                    if (employee.getWorkingDay().getLast().getCheckOutDate() != null) {
                        if (!employee.getWorkingDay().getLast().getCheckOutDate().equals(LocalDate.now())) {
                            employee.getWorkingDay().getLast().setCheckOutDate(LocalDate.parse(date));
                            //new WorkingDay(RoundTime.roundToQuarter(LocalTime.parse(hours)), LocalDate.parse(date), WorkingDay.setOffSetTimeEnding(employee, LocalTime.parse(hours)), false));
                            employee.getWorkingDay().getLast().setTimeCheckOut(RoundTime.roundToQuarter(LocalTime.parse(hours)));
                            employee.getWorkingDay().getLast().setOffSetTimeEnding(WorkingDay.setOffSetTimeEnding(employee, LocalTime.parse(hours)));
                        }
                    } else {
                        employee.getWorkingDay().getLast().setCheckOutDate(LocalDate.parse(date));
                        employee.getWorkingDay().getLast().setTimeCheckOut(RoundTime.roundToQuarter(LocalTime.parse(hours)));
                        employee.getWorkingDay().getLast().setOffSetTimeEnding(WorkingDay.setOffSetTimeEnding(employee, LocalTime.parse(hours)));
                    }
                } /*else {
                    LOGGER.info("Check-in and Check-out already done this day");
                }*/
            } else {
                employee.getWorkingDay().add(new WorkingDay(RoundTime.roundToQuarter(LocalTime.parse(hours)), LocalDate.parse(date), WorkingDay.setOffSetTimeArrival(employee, LocalTime.parse(hours)), true));
            }
        } else {
            LOGGER.info("Necessary to set hours of working day before to be able to check. Employee ID " + employee.getId());
        }
    }
}
