package com.burnier.timeclockproject.utils;

import java.io.Serializable;
import java.time.LocalTime;

public class RoundTime implements Serializable {
    /**
     * Allows to roundToQuarter every localTime
     *
     * @param dateTime the LocalTime object
     * @return the LocalTime object rounded
     * @see LocalTime
     */
    public static LocalTime roundToQuarter(LocalTime dateTime) {
        LocalTime localTime;
        int hour = dateTime.getHour();
        int minute;
        if ((dateTime.getMinute() % 15) <= 7) {
            minute = dateTime.getMinute() - (dateTime.getMinute() % 15);
        } else {
            minute = dateTime.getMinute() + (15 - dateTime.getMinute() % 15);
            if (minute == 60) {
                minute = 0;
                hour += 1;
            }
            if (hour == 24) {
                hour = 0;
            }
        }
        localTime = LocalTime.of(hour, minute);
        return localTime;
    }
}
