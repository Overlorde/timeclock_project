package com.burnier.timeclockproject.utils;

import com.burnier.timeclockproject.company.Employee;

import java.time.LocalDate;
import java.time.LocalTime;

public class WorkingDay implements java.io.Serializable {
    private LocalTime timeCheckIn;
    private LocalTime timeCheckOut;
    private int offSetTimeArrival;
    private int offSetTimeEnding;
    private LocalDate checkInDate;
    private LocalDate checkOutDate;

    /**
     * Constructor of WorkingDay
     */
    public WorkingDay() {
        timeCheckIn = null;
        timeCheckOut = null;
        offSetTimeArrival = 0;
        offSetTimeEnding = 0;
    }

    /**
     * Constructor of WorkingDay
     *
     * @param timeCheck  the time of check
     * @param checkDate the date of check
     * @param offSetTime the offset
     * @param checkIn boolean of check type
     */
    public WorkingDay(LocalTime timeCheck, LocalDate checkDate, int offSetTime, boolean checkIn) {
        if (checkIn) {
            this.timeCheckIn = timeCheck;
            this.checkInDate = checkDate;
            this.offSetTimeArrival = offSetTime;
        } else {
            this.timeCheckOut = timeCheck;
            this.checkOutDate = checkDate;
            this.offSetTimeEnding = offSetTime;
        }
    }

    /**
     * Get the date of the check in
     *
     * @return the date of the check in
     * @see LocalDate
     */
    public LocalDate getCheckInDate() {
        return checkInDate;
    }

    /**
     * Get the date of the check out
     *
     * @return the date of the check out
     * @see LocalDate
     */
    public LocalDate getCheckOutDate() {
        return checkOutDate;
    }

    /**
     * Set the date of the check in with a LocalDate
     *
     * @param localDate the object localDate
     */
    public void setCheckInDate(LocalDate localDate) {
        checkInDate = localDate;
    }

    /**
     * Set the date of the check out with a LocalDate
     *
     * @param localDate the object LocalDate
     */
    public void setCheckOutDate(LocalDate localDate) {
        checkOutDate = localDate;
    }

    /**
     * Set the offset time ending
     *
     * @param offSetTimeEnding the offset
     */
    public void setOffSetTimeEnding(int offSetTimeEnding) {
        this.offSetTimeEnding = offSetTimeEnding;
    }

    /**
     * Set the time of the check in with a LocalTime object
     *
     * @param timeCheckIn the LocalTime object
     */
    public void setTimeCheckIn(LocalTime timeCheckIn) {
        this.timeCheckIn = timeCheckIn; //Launch the time inside timeCheckIn when the button is pressed

    }

    /**
     * Set the time of the check out with a LocalTime object
     *
     * @param timeCheckOut the LocalTime object
     */
    public void setTimeCheckOut(LocalTime timeCheckOut) {
        this.timeCheckOut = timeCheckOut;
    }

    /**
     * Set the offset of the time arrival of an employee
     *
     * @param employee the employee
     */
    public static int setOffSetTimeArrival(Employee employee, LocalTime timeCheckIn) {
        //Give us the number of minutes that the employee must have in order to not be late
        return (employee.getTimeArrival().getHour() * 60 + employee.getTimeArrival().getMinute()) - (timeCheckIn.getHour() * 60 + timeCheckIn.getMinute());
    }

    /**
     * Set the offset of the time ending of an employee
     *
     * @param employee the employee
     */
    public static int setOffSetTimeEnding(Employee employee, LocalTime timeCheckOut) {
        return (employee.getTimeEnding().getHour() * 60 + employee.getTimeEnding().getMinute()) - (timeCheckOut.getHour() * 60 + timeCheckOut.getMinute());
    }

    /**
     * Get the time of the check in
     *
     * @return the LocalTime object
     * @see LocalTime
     */
    public LocalTime getTimeCheckIn() {
        return timeCheckIn;
    }

    /**
     * Get the time of the check out
     *
     * @return the LocalTime object
     * @see LocalTime
     */
    public LocalTime getTimeCheckOut() {

        return timeCheckOut;
    }

    /**
     * Get the total offset
     *
     * @return the total offset
     * @see Integer
     */
    public Integer getTotalOffSet() {
        return offSetTimeArrival + offSetTimeEnding;
    }

    /**
     * Print the informations about the check In / Check Out of the employee
     *
     * @return String
     */
    public String toString() {
        return " " + timeCheckIn + " " + checkInDate + " " + timeCheckOut + " " + checkOutDate + " " + offSetTimeArrival + " " + offSetTimeEnding;
    }
}
