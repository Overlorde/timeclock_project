package com.burnier.timeclockproject.utils;

import javafx.application.Platform;
import javafx.scene.control.Label;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static java.time.format.FormatStyle.SHORT;

public record RoundTimeRunner(Label timeLabel) implements Runnable {
    /**
     * Set the rounded time in continue
     *
     * @param timeLabel the object Label
     */
    public RoundTimeRunner {
    }

    public void run() {
        while (true) {
            LocalTime time;
            time = LocalTime.now();
            time = RoundTime.roundToQuarter(time);
            DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedTime(SHORT);
            String text = time.format(formatter);
            Platform.runLater(() -> timeLabel.setText(text));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
