package com.burnier.timeclockproject.utils;

import javafx.application.Platform;
import javafx.scene.control.Label;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static java.time.format.FormatStyle.MEDIUM;

public record Time(Label timeLabel) implements Runnable {
    /**
     * Set the time in continue
     *
     * @param timeLabel the Label object
     */
    public Time {
    }


    public void run() {
        while (true) {
            LocalTime time;
            time = LocalTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedTime(MEDIUM);
            String text = time.format(formatter);
            Platform.runLater(() -> timeLabel.setText(text));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
