package com.burnier.timeclockproject.file;

import com.burnier.timeclockproject.company.Company;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Serialization implements Runnable {
    private final Company company;

    /**
     * Constructor of Serialization
     *
     * @param company the object company
     */
    public Serialization(Company company) {
        this.company = company;
    }

    /**
     * Execution instruction of the Thread
     */
    @Override
    public void run() {
        try {
            FileOutputStream fileOut = new FileOutputStream("company.ser");
            try (ObjectOutputStream out = new ObjectOutputStream(fileOut)) {
                out.writeObject(company);
            }
            fileOut.close();
            System.out.println("Serialized data is saved in TimeClockProjectX/employee.ser\n");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }
}
