package com.burnier.timeclockproject.file;

import com.burnier.timeclockproject.company.Company;
import com.burnier.timeclockproject.company.Department;
import com.burnier.timeclockproject.company.Employee;
import com.burnier.timeclockproject.utils.WorkingDay;

import java.io.BufferedReader;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CsvFileReaderChecking {

    //Delimiter used in CSV file
    private static final String COMMA_DELIMITER = ","; //Comma must delimit the information
    private static final Logger LOGGER = Logger.getLogger(String.valueOf(CsvFileReaderChecking.class));
    //employee attributes index
    private static final int employee_ID = 0;
    private static final int employee_TIMEARRIVAL = 1;
    private static final int employee_DATEARRIVAL = 2;
    private static final int employee_TIMEENDING = 3;
    private static final int employee_DATEENDING = 4;

    /**
     * Parse the String time as a LocalTime object
     *
     * @param stringTime the time under the form of a string
     * @return the LocalTime object
     * @see LocalTime
     */
    private static LocalTime parseTime(String stringTime) {
        LocalTime localTime;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        localTime = LocalTime.parse(stringTime, formatter);
        return localTime;
    }

    /**
     * Parse the String date as a LocalDate object
     *
     * @param stringDate the date under the form of a string
     * @return the LocalDate object
     * @see LocalDate
     */
    private static LocalDate parseDate(String stringDate) {
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(stringDate, formatter2);
    }

    /**
     * Read the CVS file and add the employee list into the company
     *
     * @param fileName the file name of the file
     * @param company  the object company
     */
    public static void readCsvFileChecking(String fileName, Company company) {

        try (BufferedReader fileReader = new BufferedReader(new FileReader(fileName))) {
            String line = " ";

            //Create the file reader

            //Read the CSV file header to skip it
            fileReader.readLine();

            //Read the file line by line starting from the second line
            while ((line = fileReader.readLine()) != null) {
                //Get all tokens available in line
                String[] tokens = line.split(COMMA_DELIMITER);

                if (tokens.length > 0) {
                    for (Department department : company.getListOfDepartment()) {
                        for (Employee employee : department.getListEmployee()) {
                            if (employee.getId() == Integer.parseInt(tokens[employee_ID])) {
                                WorkingDay workingDay = new WorkingDay();
                                employee.getWorkingDay().add(workingDay);
                                employee.getWorkingDay().getLast().setTimeCheckIn(parseTime(tokens[employee_TIMEARRIVAL]));
                                employee.getWorkingDay().getLast().setCheckInDate(parseDate(tokens[employee_DATEARRIVAL]));
                                employee.getWorkingDay().getLast().setTimeCheckOut(parseTime(tokens[employee_TIMEENDING]));
                                employee.getWorkingDay().getLast().setCheckOutDate(parseDate(tokens[employee_DATEENDING]));
                                WorkingDay.setOffSetTimeEnding(employee, employee.getWorkingDay().getLast().getTimeCheckOut());
                                WorkingDay.setOffSetTimeArrival(employee, employee.getWorkingDay().getLast().getTimeCheckIn());
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Error while closing fileReaderChecking !!! ", e);
        }
    }

}
