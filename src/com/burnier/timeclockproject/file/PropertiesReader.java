package com.burnier.timeclockproject.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader implements Runnable {
    private static int port = 0;
    private static String ip;

    /**
     * Constructor of PropertiesReader
     *
     * @param port the port
     * @param ip   the ip
     */
    public PropertiesReader(int port, String ip) {
        PropertiesReader.port = port;
        PropertiesReader.ip = ip;
    }

    /**
     * Constructor of PropertiesReader
     *
     * @param port the port
     */
    public PropertiesReader(int port) {
        PropertiesReader.port = port;
    }

    @Override
    public void run() {
        final Properties prop = new Properties();
        InputStream input = null;

        try {

            input = new FileInputStream("tcp.properties");

            // load a properties file
            prop.load(input);

            ip = prop.getProperty("tcp.ip");
            port = Integer.parseInt(prop.getProperty("tcp.port"));


        } catch (final IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * Get the port
     *
     * @return the port
     * @see int
     */
    public static int getPort() {
        return port;
    }

    /**
     * Get the ip
     *
     * @return the ip
     * @see String
     */
    public static String getIp() {
        return ip;
    }
}
