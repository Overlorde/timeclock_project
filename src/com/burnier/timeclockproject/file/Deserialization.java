package com.burnier.timeclockproject.file;

import com.burnier.timeclockproject.company.Company;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Deserialization implements Runnable {
    private Company company;

    /**
     * Constructor of the thread executing
     */
    public Deserialization() {
    }

    /**
     * Execution instruction of the Thread
     */
    @Override
    public void run() {
        try {
            FileInputStream fileIn = new FileInputStream("company.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            company = (Company) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            System.out.println("Company not found");
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("Error");
        }
        System.out.println("Deserialized Company...");
    }

    /**
     * Get the company of the thread executing
     *
     * @return the object Company
     * @see Company
     */
    public Company getCompany() {
        return company;
    }

}
