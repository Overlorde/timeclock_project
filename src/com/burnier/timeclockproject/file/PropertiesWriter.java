package com.burnier.timeclockproject.file;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public record PropertiesWriter(int port, String ip) implements Runnable {
    /**
     * Constructor of PropertiesWriter
     *
     * @param port the port
     * @param ip   the ip
     */
    public PropertiesWriter {
    }

    /**
     * Execution instruction of the Thread
     */
    @Override
    public void run() {
        final Properties prop = new Properties();
        OutputStream output = null;
        try {
            output = new FileOutputStream("tcp.properties");

            // set the properties value
            prop.setProperty("tcp.port", String.valueOf(port));
            prop.setProperty("tcp.ip", ip);

            // save properties to project root folder
            prop.store(output, null);

        } catch (final IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
