package com.burnier.timeclockproject.file;

import com.burnier.timeclockproject.company.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.logging.Logger;

import static com.burnier.timeclockproject.company.Sex.UNKNOWN;

public class CsvFileReader {

    //Delimiter used in CSV file
    private static final String COMMA_DELIMITER = ","; //Comma must delimit the information
    private static final Logger LOGGER = Logger.getLogger(String.valueOf(CsvFileReader.class));

    //employee attributes index
    private static final int employee_ROLE = 0;
    private static final int employee_ID = 1;
    private static final int employee_FNAME = 2;
    private static final int employee_LNAME = 3;
    private static final int employee_GENDER = 4;
    private static final int employee_ADRESS = 5;
    private static final int employee_AGE = 6;
    private static final int employee_DEPARTMENT = 7;
    private static final int employee_TIMEARRIVAL = 8;
    private static final int employee_TIMEENDING = 9;

    /**
     * Parse the String Sex as a Sex enumeration
     *
     * @param stringSex the sex with a string form
     * @return the object Sex from the string
     * @see Sex
     */
    private static Sex parseEnum(String stringSex) {
        Sex sex;
        if (stringSex.equals("MALE")) {
            sex = Sex.MALE;
            return sex;
        } else if (stringSex.equals("FEMALE")) {
            sex = Sex.FEMALE;
            return sex;
        } else {
            return UNKNOWN;
        }
    }

    /**
     * Get the object Role from the string
     *
     * @param stringRole the role under the string form
     * @return the object Role from the string
     * @see Role
     */
    private static Role parseRole(String stringRole) {
        Role role;
        if (stringRole.equals("LEADING")) {
            role = Role.LEADING;
            return role;
        } else if (stringRole.equals("NOT_LEADING")) {
            role = Role.NOT_LEADING;
            return role;
        } else {
            role = Role.NOT_A_MANAGER;
            return role;
        }
    }

    /**
     * Read the CVS file and add the employee list into the company
     *
     * @param fileName the file name of the file
     * @param company  the object company
     */
    public static void readCsvFile(String fileName, Company company) {

        try (BufferedReader fileReader = new BufferedReader(new FileReader(fileName))) {

            //Create a new list of employee to be filled by CSV file data
            LinkedList<Employee> employees = new LinkedList<>();
            String line;

            //Create the file reader
            //Read the CSV file header to skip it
            fileReader.readLine();
            int id = 0;
            for(Department department : company.getListOfDepartment()){
                for(Employee employee : department.getListEmployee()){
                    if(employee.getId() > id){
                        id = employee.getId();
                    }
                }
            }
            //Read the file line by line starting from the second line
            while ((line = fileReader.readLine()) != null) {
                //Get all tokens available in line
                //TODO REMOVE ID FILE
                String[] tokens = line.split(COMMA_DELIMITER);
                if (tokens.length > 0) {
                    id++;
                    tokens[employee_ID] = String.valueOf(id);
                    //Create a new employee object and fill his  data

                    if (tokens[employee_DEPARTMENT] != null) {
                        if (company.searchDepartment(tokens[employee_DEPARTMENT]) == null) {
                            company.getListOfDepartment().add(new Department(tokens[employee_DEPARTMENT]));
                        }
                        if (tokens[employee_ADRESS].equals("null")) {
                            tokens[employee_ADRESS] = "";
                        }
                        if (!tokens[employee_TIMEARRIVAL].equals("null") && !tokens[employee_TIMEENDING].equals("null")) { //TODO Check correct hours format
                            if (parseRole(tokens[employee_ROLE]).equals(Role.LEADING) || parseRole(tokens[employee_ROLE]).equals(Role.NOT_LEADING)) {
                                Manager manager = new Manager(tokens[employee_FNAME], tokens[employee_LNAME], parseEnum(tokens[employee_GENDER]), parseRole(tokens[employee_ROLE]), tokens[employee_ADRESS], Integer.parseInt(tokens[employee_AGE]), company.searchDepartment(tokens[employee_DEPARTMENT]), tokens[employee_TIMEARRIVAL], tokens[employee_TIMEENDING]);
                                manager.setId(Integer.parseInt(tokens[employee_ID]));
                                if (manager.getRole().equals(Role.LEADING) && manager.getDepartment().getManager() != null) {
                                    manager.getDepartment().addManager(manager, company);
                                } else {
                                    for (Department department : company.getListOfDepartment()) {
                                        for (Employee employee : department.getListEmployee()) {
                                            if (employee.getDepartment().getNameDepartment().equals(manager.getDepartment().getNameDepartment())) {
                                                department.addEmployee(manager); //TODO FIX THREAD CONCURRENCY
                                                break;
                                            }
                                        }
                                    }
                                }
                            } else {
                                Employee employee = new Employee(tokens[employee_FNAME], tokens[employee_LNAME], parseEnum(tokens[employee_GENDER]), tokens[employee_ADRESS], Integer.parseInt(tokens[employee_AGE]), company.searchDepartment(tokens[employee_DEPARTMENT]), tokens[employee_TIMEARRIVAL], tokens[employee_TIMEENDING]);
                                employee.setRole(parseRole(tokens[employee_ROLE]));
                                employee.setId(Integer.parseInt(tokens[employee_ID]));
                                for (Department department : company.getListOfDepartment()) {
                                    for (Employee employee1 : department.getListEmployee()) {
                                        if (employee1.getDepartment().getNameDepartment().equals(employee.getDepartment().getNameDepartment())) {
                                            department.addEmployee(employee);
                                        }
                                    }
                                }
                            }
                        } else {
                            LOGGER.info("Employee not imported because timeArrival or timeEnding not defined. Please define it before importing.");
                        }
                    }
                }
            }
            for (Department department : company.getListOfDepartment()) {
                for (Employee employee : department.getListEmployee()) {
                    LOGGER.info(employee.toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
