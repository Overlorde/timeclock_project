package com.burnier.timeclockproject.file;

import com.burnier.timeclockproject.application1.ControllerTableHandler;
import com.burnier.timeclockproject.company.Company;
import com.burnier.timeclockproject.company.Department;
import com.burnier.timeclockproject.company.Employee;
import javafx.scene.control.Alert;

import java.io.FileWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CsvFileWriter {

    //Delimiter used in CSV file
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static final String FILE_HEADER = "role, id, firstName, lastName, gender, address, age, department, timeArrival, timeEnding";
    private static final Logger LOGGER = Logger.getLogger(String.valueOf(CsvFileWriter.class));
    /**
     * Write the employee of the company into a file
     *
     * @param fileName the file name of a file
     * @param company  the object company
     */
    public static void writeCsvFile(String directory, String fileName, Company company) {
        //Create new Employees objects

        try (FileWriter fileWriter = new FileWriter(directory + "/" + fileName)){
            //Write the CSV file header
            fileWriter.append(FILE_HEADER);

            //Add a new line separator after the header
            fileWriter.append(NEW_LINE_SEPARATOR);

            //Write a new Employee object list to the CSV file
            for (Department department : company.getListOfDepartment()) {
                for (Employee employee : department.getListEmployee()) {
                    fileWriter.append(String.valueOf(employee.getRole()))
                    .append(COMMA_DELIMITER)
                    .append(String.valueOf(employee.getId()))
                    .append(COMMA_DELIMITER)
                    .append(employee.getFirstName())
                    .append(COMMA_DELIMITER)
                    .append(employee.getLastName())
                    .append(COMMA_DELIMITER)
                    .append(String.valueOf(employee.getGender()))
                    .append(COMMA_DELIMITER)
                    .append(employee.getAddress())
                    .append(COMMA_DELIMITER)
                    .append(String.valueOf(employee.getAge()))
                    .append(COMMA_DELIMITER)
                    .append(employee.getNameDepartment())
                    .append(COMMA_DELIMITER)
                    .append(String.valueOf(employee.getTimeArrival()))
                    .append(COMMA_DELIMITER)
                    .append(String.valueOf(employee.getTimeEnding()))
                    .append(NEW_LINE_SEPARATOR);
                }
            }
            ControllerTableHandler.createAndShowModal(Alert.AlertType.INFORMATION,"Information Dialog","Export Employee", "CSV file has been created successfully");

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE,"Error in CsvFileWriter", e);
        }
    }
}

