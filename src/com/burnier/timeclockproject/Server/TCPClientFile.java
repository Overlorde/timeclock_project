package com.burnier.timeclockproject.Server;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;

public class TCPClientFile extends TCPClientBuilder implements Runnable {
    private LinkedList<String> listOfEmployee = new LinkedList<>();
    private final LinkedList<String> listOfEmployeeCheck;

    /**
     * Constructor of TCPClientFile
     *
     * @param listOfEmployee the list of employee
     * @param port           the port
     */
    public TCPClientFile(LinkedList<String> listOfEmployee, int port) {
        this.listOfEmployeeCheck = listOfEmployee;
        this.port = port;
    }

    @Override
    public void run() {
        try {
            setSocket();
            try (ObjectInputStream objectInputStream = new ObjectInputStream(s.getInputStream())) {
                this.listOfEmployee = (LinkedList<String>) objectInputStream.readObject();
                try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(s.getOutputStream())) {
                    objectOutputStream.writeObject(listOfEmployeeCheck);
                }
            }
            s.close();
        } catch (Exception ignored) {
        }
    }

    /**
     * Get the list of employee
     *
     * @return the list of employee
     * @see LinkedList
     */
    public LinkedList<String> getEmployee() {
        return listOfEmployee;
    }
}


