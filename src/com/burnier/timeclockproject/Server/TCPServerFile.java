package com.burnier.timeclockproject.Server;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;

public class TCPServerFile extends TCPServerBuilder implements Runnable {
    private LinkedList<String> listOfEmployee;
    private LinkedList<String> listOfCheck = new LinkedList<>();

    /**
     * Constructor of TCPServerFile
     *
     * @param listOfEmployee the list of employee
     * @param port           the port
     */
    public TCPServerFile(LinkedList<String> listOfEmployee, int port) {
        this.listOfEmployee = listOfEmployee;
        this.port = port;
    }

    /**
     * Execution instruction of the Thread
     */
    @Override
    public void run() {
        try {
            setSocket(port);
            System.out.println("TCPServerFile launched ...");
            while (true) {
                s = ss.accept();
                try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(s.getOutputStream())) {
                    objectOutputStream.writeObject(listOfEmployee);
                    try (ObjectInputStream objectInputStream = new ObjectInputStream(s.getInputStream())) {
                        this.listOfCheck = (LinkedList<String>) objectInputStream.readObject();
                    }
                }
                s.close();
            }
        } catch (Exception var4) {
            try {
                ss.close();
            } catch (Exception var3) {
                System.out.println("Exception TCPServerFile");
            }

        }

    }

    /**
     * Get the list of employee
     *
     * @return the list of employee
     * @see LinkedList
     */
    public LinkedList<String> getEmployee() {
        return listOfEmployee;
    }

    /**
     * Get the list of employee's check
     *
     * @return the employee's check
     * @see LinkedList
     */
    public LinkedList<String> getListOfCheck() {
        return listOfCheck;
    }

    /**
     * Set the list of employee
     *
     * @param listOfEmployee the list of employee
     */
    public void setListOfEmployee(LinkedList<String> listOfEmployee) {
        this.listOfEmployee = listOfEmployee;
    }
}
