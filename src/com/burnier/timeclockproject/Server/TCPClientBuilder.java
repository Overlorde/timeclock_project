package com.burnier.timeclockproject.Server;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class TCPClientBuilder {
    protected Socket s;
    private static int count;
    private InetSocketAddress isA;
    protected int port;
    private final String ip = "127.0.0." + count; //TODO Check not working connection

    /**
     * Constructor of TCPClientBuilder
     */
    TCPClientBuilder() {
        s = null;
        if (count == 254) {
            count = 0;
        }
        count++;
        isA = null;
    }

    /**
     * Set the socket of the client
     * @throws IOException an IOException
     */
    void setSocket() throws IOException {
        isA = new InetSocketAddress(ip, port);
        s = new Socket(isA.getHostName(), isA.getPort());
        s.setSoTimeout(5000);
    }

    /**
     * Get the port
     * @return the port
     * @see int
     */
    public int getPort() {
        return port;
    }

    /**
     * Set the port
     * @param port the port
     */
    public void setPort(int port) {
        this.port = port;
    }
}
