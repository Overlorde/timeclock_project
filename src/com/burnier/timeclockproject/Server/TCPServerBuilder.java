package com.burnier.timeclockproject.Server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServerBuilder {
    protected Socket s;
    private String ip;
    protected int port = 8080;
    private InetSocketAddress isA;
    ServerSocket ss;

    /**
     * Constructor of TCPServerBuilder
     */
    TCPServerBuilder() {
        ss = null;
        ip = "localhost";
        s = null;
        isA = new InetSocketAddress(ip, port);
    }

    /**
     * Set the server socket
     *
     * @param port the port
     * @throws IOException an IOException
     */
    void setSocket(int port) throws IOException {
        isA = new InetSocketAddress("localhost", port);
        ss = new ServerSocket(isA.getPort());
    }

    /**
     * Set the ip
     *
     * @param ip the ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * Get the port
     *
     * @return the port
     * @see int
     */
    public int getPort() {
        return port;
    }

    /**
     * Set the port
     *
     * @param port the port
     */
    public void setPort(int port) {
        this.port = port;
    }
}
