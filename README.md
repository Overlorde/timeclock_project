# TIMECLOCK JAVA APPLICATION


### Prerequisites

A minimum version of Java [8](https://www.oracle.com/fr/java/technologies/javase/javase-jdk8-downloads.html) is required to run the application.
Any version JavaFx is compatible but mandatory. 

### Configurations

The files are created or edited in the project root in default configuration
* tcp.properties is used in order to set the ip and port of the timeclock and main application
* text.txt is an example of a file where employee have been exported from the main application
* checking.txt is an example of a file with already check in and check out  waiting to be set in the main application
* company.ser is an example of a company already serialized

### Informations

**The timeclock must be connected at least one time to the main application in order to record the check-in and check-out
else the data will be lost after closing the timeclock. The timeclock can keep the record if disconnected from main application until it reconnect.**

**Checking will not be recorded for the employee if working hours are not set.**
**tcp.properties must be present in order to synchronise port and ip between main application and timeclock.**

***Application can handle multiples and simultaneous timeclock connection.**

### About the application

- Both applications are using tcp.properties in order to synchronize their port and ip so do not remove it.
- The main application is consider as the server (TCPServer) and get the information of the timeclock which is considered as a client (TCPClient). The main application send the id and names of employees and get the checking of the timeclock.
- A thread is used for the server and the client and the client is refresh every 2 seconds by default. A thread is used for the class time which refresh the screen of the timeclock with the current time. A thread is used for the class roundTime which refresh the screen of the timeclock with the current time rounded
- A thread is used for serialization and deserialization for instance when the main application is closed or when both applications are starting

## Tests

Tests can be found here:
```bash
timeclock_project\src\com\burnier\timeclockproject\test
```

## Versioning

Version 1.0
Edit : 2018-06-02

## Authors

* **Burnier-Framboret Alexandre** - *Initial work* - [Overlorde](https://gitlab.com/Overlorde)

## License

This project is not under license.

## Acknowledgments

* Created with Java love